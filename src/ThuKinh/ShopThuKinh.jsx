import React, { Component } from 'react'
import NguoiMau from './NguoiMau';
import DanhSach from './DanhSach';

export default class ShopThuKinh extends Component {
    state = {
      
}
    
    chonKinh = (img, name, price, desc) => {
        this.setState({
            imageUrl: img,
            name: name,
            price: price,
            desc: desc
        })
    }

    listGlasses = [
        {
            id: 1,
            price: 30,
            name: "GUCCI G8850U",
            url: "./image_source/glassesImage/v1.png",
            desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },

        {
            id: 2,
            price: 50,
            name: "GUCCI G8759H",
            url: "./image_source/glassesImage/v2.png",
            desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        },

        {
          id: 3,
          price: 60,
          name: "GUCCI G8759H",
          url: "./image_source/glassesImage/v3.png",
          desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
      },
      {
        "id": 4,
        "price": 70,
        "name": "DIOR D6005U",
        "url": "./image_source/glassesImage/v4.png",
        "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
    },
    {
      "id": 5,
      "price": 40,
      "name": "PRADA P8750",
      "url": "./image_source/glassesImage/v5.png",
      "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
  },
  {
      "id": 6,
      "price": 60,
      "name": "PRADA P9700",
      "url": "./image_source/glassesImage/v6.png",
      "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
  },
  {
      "id": 7,
      "price": 80,
      "name": "FENDI F8750",
      "url": "./image_source/glassesImage/v7.png",
      "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
  },
  {
      "id": 8,
      "price": 100,
      "name": "FENDI F8500",
      "url": "./image_source/glassesImage/v8.png",
      "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
  },
  {
      "id": 9,
      "price": 60,
      "name": "FENDI F4300",
      "url": "./image_source/glassesImage/v9.png",
      "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
  }
    ]
    
    render() {
    let {imageUrl, name, price, desc} = this.state;
    return (
      <div>
 <div className="container vglasses py-3">
  <div className="row  justify-content-between">
    <div className="col-6 vglasses__left">
       <DanhSach chonKinh={this.chonKinh} listGlasses={this.listGlasses}/>
      <div className="row" id="vglassesList">
        
      </div>
    </div>
    <div className="col-5 vglasses__right p-0">
      <div className="vglasses__card">
        <div className="mb-2 text-right mt-2 mr-2">
          
        </div>
        <NguoiMau img={imageUrl} name={name} price={price} desc={desc}/>
        
      </div>
    </div>
  </div>
</div>

      </div>
    )
  }
}
