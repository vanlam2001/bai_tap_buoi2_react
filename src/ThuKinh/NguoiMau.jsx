import React, { Component } from 'react'

export default class NguoiMau extends Component {
  render() {
    let { img, name, price, desc } = this.props
    return (
      <div>
        <div className='vglasses__model'>
        <img src={img} alt=""/>
        
        </div>
        
        <h4>{name}</h4>
        <h4>{price}$</h4>
        <span className="text-success">Còn hàng</span>
        <p>{desc}</p>
      </div>
      
    )
  }
}

