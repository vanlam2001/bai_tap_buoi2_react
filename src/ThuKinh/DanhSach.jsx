import React, { Component } from 'react'
import Item from './Item'
export default class DanhSach extends Component {
  renderDanhSach = (list) => {
    return list.map((glass, index) => {
      return <Item chonKinh={this.props.chonKinh} key={index} img={glass.url} name={glass.name} price={glass.price} desc={glass.desc} />
    })
  }
  render() {
    let { listGlasses } = this.props;
    return (
      <div>
        <div className="row">
          <div className="col-12">
            <h1 className="mb-2">Thử kính</h1>
          </div>
          {this.renderDanhSach(listGlasses)}

        </div>
      </div>
    )
  }
}
