import './App.css';
import ShopThuKinh from './ThuKinh/ShopThuKinh';

function App() {
  return (
    <div className="App">
      <ShopThuKinh/>
    </div>
  );
}

export default App;
